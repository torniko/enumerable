﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EnumerableTask
{
    public class EnumerableManipulation
    {
        /// <summary> Transforms all strings to upper case.</summary>
        /// <param name="data">Source string sequence.</param>
        /// <returns>
        ///   Returns sequence of source strings in uppercase.
        /// </returns>
        /// <example>
        ///    {"a", "b", "c"} => { "A", "B", "C" }
        ///    { "A", "B", "C" } => { "A", "B", "C" }
        ///    { "a", "A", "", null } => { "A", "A", "", null }
        /// </example>
        public IEnumerable<string> GetUppercaseStrings(IEnumerable<string> data)
        {
            var enumer = data.GetEnumerator();
            List<string> myList = new List<string>();
            while (enumer.MoveNext())
            {
                StringBuilder sb = new StringBuilder();
                if (string.IsNullOrEmpty(enumer.Current))
                {
                    if (enumer.Current == null)
                    {
                        myList.Add(null);
                    }
                    else
                    {
                        myList.Add(enumer.Current);
                    }
                    continue;
                }
                for (int i = 0; i < enumer.Current.Length; i++)
                {
                    if (char.IsLower(enumer.Current[i])
)
                    {
                        sb.Append(enumer.Current[i].ToString().ToUpper());
                    }
                    else
                    {
                        sb.Append(enumer.Current[i].ToString());
                    }
                }
                myList.Add(sb.ToString());
            }
            return myList;

        }

        /// <summary> Transforms an each string from sequence to its length.</summary>
        /// <param name="data">Source strings sequence.</param>
        /// <returns>
        ///   Returns sequence of strings length.
        /// </returns>
        /// <example>
        ///   { } => { }
        ///   {"a", "aa", "aaa" } => { 1, 2, 3 }
        ///   {"aa", "bb", "cc", "", "  ", null } => { 2, 2, 2, 0, 2, 0 }
        /// </example>
        public IEnumerable<int> GetStringsLength(IEnumerable<string> data)
        {
            var enumer = data.GetEnumerator();
            List<int> myList = new List<int>();
            while (enumer.MoveNext())
            {
                if (string.IsNullOrEmpty(enumer.Current))
                {
                    myList.Add(0);

                    continue;
                }
                myList.Add(enumer.Current.Length);
            }
            return myList;
        }
        public IEnumerable<long> GetSquareSequence(IEnumerable<int> data)
        {
            var enumer = data.GetEnumerator();
            List<long> myList = new List<long>();
            while (enumer.MoveNext())
            {
                long res = (long)enumer.Current * (long)enumer.Current;
                myList.Add(res);
            }
            return myList;
        }
        public IEnumerable<string> GetPrefixItems(IEnumerable<string> data, string prefix)
        {
            if (prefix == null)
            {
                throw new ArgumentNullException(nameof(prefix),"Prefix cannot be null");
            }
            var enumer = data.GetEnumerator();
            List<string> myList = new List<string>();
            while (enumer.MoveNext())
            {
                if (enumer.Current == null)
                {
                    continue;
                }
                string current = enumer.Current.ToLower();

                if (current.StartsWith(prefix.ToLower()))
                {
                    myList.Add(enumer.Current);
                }
            }
            return myList;
        }
        public IEnumerable<int> Get3LargestItems(IEnumerable<int> data)
        {
            int first = 0;
            int second = 0;
            int third = 0;
            ICollection<int> newData = data as ICollection<int>;
            List<int> myList = new List<int>();
            foreach (var item in data)
            {
                if (item > first)
                {
                    third = second;
                    second = first;
                    first = item;
                }
                else if (item > second)
                {
                    third = second;
                    second = item;
                }
                else if (item > third)
                {
                    third = item;
                }

            }
            if (newData.Count == 0)
            {
                return myList;
            }
            else if (newData.Count == 2)
            {
                myList.Add(first);
                myList.Add(second);
            }
            else
            {
                myList.Add(first);
                myList.Add(second);
                myList.Add(third);
            }
            

            return myList;


        }
        public int GetSumOfAllIntegers(object[] data)
        {
            int sum = 0;
            foreach(var item in data)
            {
                if (item is int)
                {
                    sum += (int)item;
                }
            }
            return sum;
        }

    }
}