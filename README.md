## Task description ##


- Implement the methods for working with various collections that derive from `IEnumerable<T>` interface. 
 The task definitions are given in the XML-comments for each method. 

- Implement the `GetUppercaseStrings` method converting each item of the collection to upper case. 

- Implement the `GetStringsLength` method which transforms the input collection to another collection. Each element of the output collection should contain the length of the matching element in the input collection. 

- Implement the rest of the methods according to the xml description provided for each of them. 

- Build a solution in [Visual Studio](https://docs.microsoft.com/en-us/visualstudio/ide/building-and-cleaning-projects-and-solutions-in-visual-studio?view=vs-2019). Make sure there are no compiler errors and warnings, fix these issues and rebuild the solution. 

- Run all unit tests with [Visual Studio](https://docs.microsoft.com/en-us/visualstudio/test/run-unit-tests-with-test-explorer?view=vs-2019) and make sure there are no failed unit tests. Fix your code to [make all tests GREEN](https://stackoverflow.com/questions/276813/what-is-red-green-testing). 

- Review all your changes in the codebase **before** [staging the changes and creating a commit](https://docs.microsoft.com/en-us/azure/devops/repos/git/commits?view=azure-devops&tabs=visual-studio). 

 - [Stage your changes, create a commit](https://docs.microsoft.com/en-us/azure/devops/repos/git/commits?view=azure-devops&tabs=visual-studio), and publish your changes to the remote repository. 

 
**Additional Materials**

Feel welcome to check out a set of supplementary articles from C# reference: 

 

- [IEnumerable<T> interface](https://docs.microsoft.com/en-us/dotnet/api/system.collections.generic.ienumerable-1?view=net-5.0)

- [String.ToUpper method](https://docs.microsoft.com/en-us/dotnet/api/system.string.toupper?view=net-5.0)

 


 
 
